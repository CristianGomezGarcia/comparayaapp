import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { PagesRoutingModule } from './pages-router.module';
import { SupportComponent } from './support/components/support/support.component';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        PagesRoutingModule
    ],
    declarations: [SupportComponent],
    exports: [],
    schemas: [NO_ERRORS_SCHEMA]
})
export class PagesModule { }
