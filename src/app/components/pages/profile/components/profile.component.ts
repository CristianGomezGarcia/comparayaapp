import { Component, OnInit } from '@angular/core';
import { ApplicationSettings, Page } from '@nativescript/core';
import { ClientModel } from '~/app/core/Models/Client/Client@Model';
import { DataClientService } from '~/app/core/Services/DataClient/dataclient.service';

@Component({
    selector: 'app-profile',
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.scss']
})

export class ProfileComponent implements OnInit {
    
    dataClientArr: ClientModel;
    userActive: number;

    constructor(
        private dataClientService: DataClientService,
        private page: Page
    ) {
        this.userActive = ApplicationSettings.getNumber('userActive');
        this.page.actionBarHidden = true;
        this.getDataClient();
    }

    ngOnInit() { }

    getDataClient() {
        let arrayDataClient: ClientModel;
        this.dataClientService.getDataClient(this.userActive)
            .subscribe((data: ClientModel) => {
                arrayDataClient = data;
            }, error => {
                console.error('Error Profile Get Data Client => ', error.message);
            }, () => {
                this.dataClientArr = arrayDataClient;
            })
    }
}