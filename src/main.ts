// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic } from "@nativescript/angular";

import { AppModule } from "./app/app.module";

import { Theme } from '@nativescript/theme';

import { registerElement } from '@nativescript/angular';
import { Gif } from 'nativescript-gif';
registerElement('Gif', () => Gif);

Theme.setMode(Theme.Light);

platformNativeScriptDynamic().bootstrapModule(AppModule);
